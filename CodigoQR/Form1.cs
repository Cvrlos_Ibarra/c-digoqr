﻿using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodigoQR
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Btnpreview_Click(object sender, EventArgs e)
        {
            btnGuardar.Enabled = true;

            QrEncoder qrEncoder = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode = new QrCode();
            qrEncoder.TryEncode(txtValor.Text, out qrCode);
            GraphicsRenderer renderer = new GraphicsRenderer(new FixedCodeSize(400,QuietZoneModules.Zero),Brushes.Black,Brushes.White);
            MemoryStream ms = new MemoryStream();

            renderer.WriteToStream(qrCode.Matrix,ImageFormat.Png,ms);
            var imageTemporal = new Bitmap(ms);
            var imagen = new Bitmap(imageTemporal, new Size(new Point(200,200)));
            pnlCodigo.BackgroundImage = imagen;

            //Guardar en el SSD la imagen (Carpeta del proyecto)
            imagen.Save("imagen.png", ImageFormat.Png);
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            Image img = (Image)pnlCodigo.BackgroundImage.Clone();

            SaveFileDialog CajaDialogo = new SaveFileDialog();
            CajaDialogo.AddExtension = true;
            CajaDialogo.Filter = "Image PNG (*.png)|*.png";
            CajaDialogo.ShowDialog();
            CajaDialogo.FileName = txtValor.Text;
            if (!string.IsNullOrEmpty(CajaDialogo.FileName))
            {
                img.Save(CajaDialogo.FileName, ImageFormat.Png);
            }
            img.Dispose();
        }
    }
}
